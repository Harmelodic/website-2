<img src="https://github.com/Harmelodic/www_v2/blob/master/Harmelodic.png" alt="Logo" height="100">

# www v2

The Harmelodic website (v2) (Static)

This is the second version of the website that I built for myself.

It uses the standard HTML, CSS and JavaScript structure of static sites and uses JQuery for its animations.

##### Runs in: Apache HTTP Server
