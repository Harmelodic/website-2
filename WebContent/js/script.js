$(document).ready(function(){
	if ($("#mobile-indicator").is(":visible")){
		var menuOpen = false;
		$("#sidebarButton").click(function(){
			if (menuOpen == false){
				$("#sidebar").animate({left: "0px"}, 200, "swing");
				$(this).css("background", 'url("images/sidebarButtonOpen.png")');
				$("#content").fadeTo(200, 0.6, "swing");
				$(".projectLink").css("pointer-events", "none");
				menuOpen = true;
			}
			else {
				$("#sidebar").animate({left: "-60%"}, 200, "swing");
				$(this).css("background", 'url("images/sidebarButtonClose.png")');
				$("#content").fadeTo(200, 1, "swing");
				$(".projectLink").css("pointer-events", "auto");
				menuOpen = false;
			}
	    });
	}
	else{
		var menuOpen = false;
		$("#sidebarButton").click(function(){
			if (menuOpen == false){
				$("#sidebar").animate({left: "0px"}, 200, "swing");
				$(this).css("background", 'url("images/sidebarButtonOpen.png")');
				$("#content").fadeTo(200, 0.6, "swing");
				$(".projectLink").css("pointer-events", "none");
				menuOpen = true;
			}
			else {
				$("#sidebar").animate({left: "-300px"}, 200, "swing");
				$(this).css("background", 'url("images/sidebarButtonClose.png")');
				$("#content").fadeTo(200, 1, "swing");
				$(".projectLink").css("pointer-events", "auto");
				menuOpen = false;
			}
	    });
	};
});
